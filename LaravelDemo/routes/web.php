<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
use App\TheLoai;
Route::get('/', function () {
    return view('welcome');
});

//Route::get('thu', function (){
//   $theloai = TheLoai::find(1);
//   foreach ($theloai->loaitin as $loaitin){
//       echo $loaitin->Ten."<br/>";
//   }
//});

//Route khai báo phần admin đăng nhập..Mục đích khai báo Route đăng nhập bên ngoài là vì sau sẽ dùng middleware để kiểm tra..Nếu đăng nhập rồi thì cho vào Route admin còn chưa đăng nhập thì ko cho vào
Route::get('admin/login','UserController@getLoginAdmin');
Route::post('admin/login','UserController@postLoginAdmin');
Route::get('admin/logout','UserController@getLogout');

Route::group(['prefix'=>'admin','middleware'=>'adminLogin'], function (){
    //admin/theloai/danhsach
     Route::group(['prefix'=>'theloai'], function (){
          Route::get('danhsach','TheLoaiController@getDanhSach');
          Route::get('sua/{id}','TheLoaiController@getSua');
          Route::post('sua/{id}','TheLoaiController@postSua');
          Route::get('them','TheLoaiController@getThem');
          Route::post('them','TheLoaiController@postThem');
          Route::get('xoa/{id}','TheLoaiController@getXoa');
     });
    //admin/loaitin/danhsach
    Route::group(['prefix'=>'loaitin'], function (){
        Route::get('danhsach','LoaiTinController@getDanhSach');
        Route::get('sua/{id}','LoaiTinController@getSua');
        Route::post('sua/{id}','LoaiTinController@postSua');
        Route::get('them','LoaiTinController@getThem');
        Route::post('them','LoaiTinController@postThem');
        Route::get('xoa/{id}','LoaiTinController@getXoa');
    });

    //admin/tintuc/danhsach
    Route::group(['prefix'=>'tintuc'], function (){
        Route::get('danhsach','TinTucController@getDanhSach');
        Route::get('them','TinTucController@getThem');
        Route::post('them','TinTucController@postThem');
        Route::get('sua/{id}','TinTucController@getSua');
        Route::post('sua/{id}','TinTucController@postSua');
        Route::get('xoa/{id}','TinTucController@getXoa');
    });
    //admin/tintuc/comment
    Route::group(['prefix'=>'comment'], function (){
        Route::get('xoa/{id}/{idTinTuc}','CommentController@getXoa');/*Truyền thêm idTinTuc để sang bên controller sử dụng*/
    });


    //admin/slide/danhsach
    Route::group(['prefix'=>'slide'], function (){
        Route::get('danhsach','SlideController@getDanhSach');
        Route::get('them','SlideController@getThem');
        Route::post('them','SlideController@postThem');
        Route::get('sua/{id}','SlideController@getSua');
        Route::post('sua/{id}','SlideController@postSua');
        Route::get('xoa/{id}','SlideController@getXoa');
    });

    //admin/user/danhsach
    Route::group(['prefix'=>'user'], function (){
        Route::get('danhsach','UserController@getDanhSach');
        Route::get('them','UserController@getThem');
        Route::post('them','UserController@postThem');
        Route::get('sua/{id}','UserController@getSua');
        Route::post('sua/{id}','UserController@postSua');
        Route::get('xoa/{id}','UserController@getXoa');
    });

    //Khai báo 1 group ajax riêng cho phần thể loại
    Route::group(['prefix'=>'ajax'], function (){
       Route::get('loaitin/{idTheLoai}','AjaxController@getLoaiTin');
    });

});
/*Goi đến controller xử lý và trả về trang chủ*/
Route::get('trangchu','PagesController@trangchu');
Route::get('contact','PagesController@contact');
Route::get('loaitin/{id}/{TenKhongDau}.html','PagesController@loaitin');
Route::get('tintuc/{id}/{TieuDeKhongDau}.html','PagesController@tintuc');

/*-------Đăng nhập user-----------*/
Route::get('login_user','PagesController@getLogin_user');
Route::post('login_user','PagesController@postLogin_user');
Route::get('logout_user','PagesController@getLogout_user');
/*-------------Comment------------*/
Route::post('comment/{id}','CommentController@postComment');
/*chi tiết người dùng*/
Route::get('nguoidung', 'PagesController@getNguoidung');
Route::post('nguoidung', 'PagesController@postNguoidung');
/*Trang giới thiệu*/
Route::get('gioithieu', 'PagesController@getGioiThieu');
/*Trang đăng ký người dùng*/
Route::get('dangky', 'PagesController@getDangKy');
Route::post('dangky', 'PagesController@postDangKy');
/*Tìm kiếm tin tức*/
Route::get('timkiem', 'PagesController@getTimKiem');
Route::get('loiTimKiem', 'PagesController@getloiTimKiem');