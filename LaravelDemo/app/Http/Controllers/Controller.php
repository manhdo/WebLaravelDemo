<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;/*Thư viện Hỗ trợ việc đăng nhập Auth trong laravel*/

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    function __construct(){
        $this->login_Admin();/*Mỗi lần gọi đến controller bất kỳ đều goi controller này ra..Gọi đến controller này ra thì lại gọi $this->login này .Gọi xong nó sẽ đều gọi đến kiểm tra xem có đăng nhập hay không*/
    }
    /*Dùng để kiểm tra xem chúng ta có đang đăng nhập hay không*/
    function login_Admin(){
        if(Auth::check()){
            view()->share('user_login', Auth::user());/*Tất cả view nào đang chạy sẽ đều có view share này*/
        }
    }

}
