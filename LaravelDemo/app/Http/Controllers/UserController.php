<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;/*Thư viện Hỗ trợ việc đăng nhập Auth trong laravel*/
use App\User;
use App\Comment;
use DB;
class UserController extends Controller
{
    //
    public function getDanhSach(){
        $user = User::all();
        return view('admin.user.danhsach',['user'=>$user]);
    }

    public function getThem(){
        return view('admin.user.them');
    }
    /*Thêm dữ liệu vào danh sách*/
    public function postThem(Request $request){
        $this->validate($request,[
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users,email',
            'password'=>'required|min:3|max:32',
            'passwordAgain'=>'required|same:password'
        ],
            [
                'name.required'=>'Bạn chưa nhập tên người dùng',
                'name.min'=>'Tên người dùng phải có ít nhất 3 kí tự',
                'email.required'=>'Bạn chưa nhập email người dùng',
                'email.email'=>'Bạn chưa nhập đúng định dạng email',
                'email.unique'=>'Email đã tồn tại',
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'Mật khẩu phải có ít nhất 3 kí tự',
                'password.max'=>'Mật khẩu chỉ được tối đa 32 kí tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp mật khẩu trên',
            ]);
        $user= new User();
        $user->name = $request->name;
        $user->email =  $request->email;
        $user->password =  bcrypt($request->password);
        $user->quyen =  $request->quyen;
        $user->save();
        return redirect('admin/user/them')->with('thongbao','Bạn đã đăng ký thành công');
    }

    public function getSua($id){
        $user = User::find($id);
        return view('admin.user.sua',['user'=>$user]);
    }

    public function postSua(Request $request , $id){
        $this->validate($request,[
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users,email',
        ],
            [
                'name.required'=>'Bạn chưa nhập tên người dùng',
                'name.min'=>'Tên người dùng phải có ít nhất 3 kí tự',
                'email.required'=>'Bạn chưa nhập email người dùng',
                'email.email'=>'Bạn chưa nhập đúng định dạng email',
                'email.unique'=>'Email đã tồn tại',
            ]);
        $user = User::find($id);
        $user->name = $request->name;
        $user->email =  $request->email;
        $user->quyen =  $request->quyen;
        if($request->changePassword == "on"){
            $this->validate($request,[
                'password'=>'required|min:3|max:32',
                'passwordAgain'=>'required|same:password'
            ],
                [
                    'password.required'=>'Bạn chưa nhập mật khẩu',
                    'password.min'=>'Mật khẩu phải có ít nhất 3 kí tự',
                    'password.max'=>'Mật khẩu chỉ được tối đa 32 kí tự',
                    'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                    'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp mật khẩu trên',
                ]);
            $user->password =  bcrypt($request->password);
        }
        $user->save();
        return redirect('admin/user/sua/'.$id)->with('thongbao','Sửa thành công');
    }



    public function getXoa($id){
        $user = User::find($id);
        $comment = Comment::where('idUser',$id); //Tìm các comment của user
        $comment->delete(); //Xóa các comment của user
        $user->delete(); //Xóa user
        return redirect('admin/user/danhsach')->with('thongbao','Bạn đã xóa thành công User');
    }



    /*Hàm đăng nhập admin*/
    public function getLoginAdmin(){
        return view('admin.login');
    }
    public function postLoginAdmin(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required|min:3|max:32'
        ],
        [
            'email.required'=>'Bạn chưa đăng nhập email',
            'password.required'=>'Bạn chưa nhập password',
            'password.min'=>'Password phải có ít nhất 3 kí tự và không quá 32 kí tự',
            'password.max'=>'Password phải có ít nhất 3 kí tự và không quá 32 kí tự'
        ]);
        //attempt là hàm chứng thực user.Phương thức attempt đc gọi khi auth.attempt đc thực thi.Nếu chứng thực thành công thì sẽ vào logger
        if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password])){
            return redirect('admin/theloai/danhsach');
        }else{
            return redirect('admin/login')->with('thongbao','Đăng nhập không thành công');
        }
    }
    /*Hàm đăng xuất admin*/
    public  function getLogout(){
        Auth::logout();
        return redirect('admin/login');
    }
}
