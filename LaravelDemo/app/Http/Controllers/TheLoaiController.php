<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
//use Illuminate\Validation\Rules\Unique;/*Dùng cái này rõ ràng cho việc check Unique.Còn dùng ValidateRequests rõ ràng cho việc check validate input nhập vào*/
//use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;/*Còn dùng import thư viện này là Controller đã bao gồm toàn bộ các thư viện.Trong đó có cả ValidateRequest.(Mà Trong ValidateRequest lại bao gồm thư viện Unique)*/

class TheLoaiController extends Controller
{
    //
    public function getDanhSach(){
        //return view('admin.theloai.danhsach');
        $theloai = TheLoai::all();//Hàm lấy tất cả các thể loại là hàm all(); chúng ta ko cần phải select truy vấn nữa mà model nó đã thực hiện hết cho chúng ta
        return view('admin.theloai.danhsach',['theloai'=>$theloai]);/*truyền dữ liệu sang trang theloai.danhsach*/
    }
    public function getThem(){
        return view('admin.theloai.them');
    }
    /*Thêm dữ liệu vào danh sách*/
    public function postThem(Request $request){
//        echo $request->Ten;
        $this->validate($request,[
            //'Ten' => 'required|min:3|max:100'
            'Ten' => 'required|unique:TheLoai,Ten|min:3|max:100'
        ],
        [
            'Ten.required'=>'Bạn chưa nhập thể loại',
            'Ten.unique'=> 'Tên thể loại đã tồn tại',
            'Ten.min' => 'Tên thể loại phải có độ dài từ 3 đến 100 kí tự',
            'Ten.max' => 'Tên thể loại phải có độ dài từ 3 đến 100 kí tự',
        ]);
        $theloai = new TheLoai();//Khởi tạo 1 đối tượng mới để lưu vào csdl
        $theloai->Ten = $request->Ten;
        $theloai->TenKhongDau = changeTitle($request->Ten);
        //echo changeTitle($request->Ten);
        $theloai->save();/*Lưu thể loại lại*/
        return redirect('admin/theloai/them')->with('thongbao','Bạn đã thêm thành công');
    }
    public function getSua($id){
        $theloai = TheLoai::find($id);//Tìm thể loại có id = $id mình truyền vào;
        return view('admin.theloai.sua',['theloai'=>$theloai]);
    }

    public function postSua(Request $request , $id){/*Lấy id về để biết sửa tại thể loại nào*/
        $theloai = TheLoai::find($id);//Tìm thể loại có id = $id mình truyền vào;
        $this->validate($request,[
            'Ten' => 'required|unique:TheLoai,Ten|min:3|max:100'
        ],
            [
                'Ten.required'=>'Bạn chưa nhập thể loại',
                'Ten.unique'=> 'Tên thể loại đã tồn tại',
                'Ten.min' => 'Tên thể loại phải có độ dài từ 3 đến 100 kí tự',
                'Ten.max' => 'Tên thể loại phải có độ dài từ 3 đến 100 kí tự',
            ]);
        $theloai->Ten = $request->Ten;
        $theloai->TenKhongDau = changeTitle($request->Ten);
        //echo changeTitle($request->Ten);
        $theloai->save();/*Lưu thể loại lại*/
        return redirect('admin/theloai/sua/'.$id)->with('thongbao','Bạn đã sửa thành công');
    }
    public function getXoa($id){
        $theloai = TheLoai::find($id);//Tìm thể loại có id = $id mình truyền vào;
        $theloai -> delete();
        return redirect('admin/theloai/danhsach')->with('thongbao','Bạn đã xóa thành công');
    }
}
