<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slide;

class SlideController extends Controller
{
    //
    public function getDanhSach(){
        $slide = Slide::all();
        return view('admin.slide.danhsach',['slide'=>$slide]);
    }

    public function getThem(){
        return view('admin.slide.them');
    }
    /*Thêm dữ liệu vào danh sách*/
    public function postThem(Request $request){
        $this->validate($request,[
                'Ten' => 'required',
                'NoiDung' => 'required'
            ],
            [
                'Ten.required'=>'Bạn chưa nhập tên',
                'NoiDung'=>'Bạn chưa nhập Nội dung'
        ]);
        $slide = new Slide();
        $slide->Ten = $request->Ten;
        $slide->NoiDung =  $request->NoiDung;
        if($request->has('link'))
            $slide->link = $request->link;
        if($request->hasFile('Hinh')){
            $file = $request->file('Hinh');
            $duoi_anh = $file->getClientOriginalExtension();/*Kiểm tra đuôi mở rộng của ảnh(png,jpg) trc khi thêm*/
            if($duoi_anh != 'jpg' && $duoi_anh!='png' && $duoi_anh!='jpeg' && $duoi_anh != 'gif'){
                return redirect('admin/slide/them')->with('loi_anh','Bạn chỉ được upload đúng dạng file ảnh ,đuôi png, jpg,jpeg,gif');
            }
            $name = $file->getClientOriginalName();/*Lấy tên của hình ra*/
            $Hinh = str_random(4)."_".$name;
            while (file_exists("upload/slide/".$Hinh))/*Thêm vòng lặp while để random tránh việc 4 số đầu random_đến số tiếp theo vẫn bị trùng*/
            {
                $Hinh = str_random(4)."_".$name;
            }
            $file->move("upload/slide",$Hinh);
            $slide->Hinh = $Hinh;//Lưu lại $Hinh vào $tintuc
        }else{
            $slide->Hinh = "";
        }
        $slide->save();
        return redirect('admin/slide/them')->with('thongbao','Thêm slide thành công');
    }

    public function getSua($id){
        $slide = Slide::find($id);
        return view('admin.slide.sua',['slide'=>$slide]);
    }

    public function postSua(Request $request , $id){
        $slide = Slide::find($id);
        $this->validate($request,[
            'Ten' => 'required',
            'NoiDung' => 'required'
        ],
            [
                'Ten.required'=>'Bạn chưa nhập tên',
                'NoiDung'=>'Bạn chưa nhập Nội dung'
            ]);
        $slide->Ten = $request->Ten;
        $slide->NoiDung =  $request->NoiDung;
        if($request->has('link'))
            $slide->link = $request->link;
        if($request->hasFile('Hinh')){
            $file = $request->file('Hinh');
            $duoi_anh = $file->getClientOriginalExtension();/*Kiểm tra đuôi mở rộng của ảnh(png,jpg) trc khi thêm*/
            if($duoi_anh != 'jpg' && $duoi_anh!='png' && $duoi_anh!='jpeg' && $duoi_anh != 'gif'){
                return redirect('admin/slide/them')->with('loi_anh','Bạn chỉ được upload đúng dạng file ảnh ,đuôi png, jpg,jpeg,gif');
            }
            $name = $file->getClientOriginalName();/*Lấy tên của hình ra*/
            $Hinh = str_random(4)."_".$name;
            while (file_exists("upload/slide/".$Hinh))/*Thêm vòng lặp while để random tránh việc 4 số đầu random_đến số tiếp theo vẫn bị trùng*/
            {
                $Hinh = str_random(4)."_".$name;
            }
            $file->move("upload/slide",$Hinh);
            /*Xóa hình cũ đi khi ta đã chỉnh sửa để lưu hình mới lại*/
            unlink("upload/slide/".$slide->Hinh);
            $slide->Hinh = $Hinh;//Lưu lại $Hinh vào $tintuc
        }else{
            $slide->Hinh = "";
        }
        $slide->save();
        return redirect('admin/slide/sua/'.$id)->with('thongbao','Bạn đã sửa slide thành công');
    }

    public function getXoa($id){
        $slide = Slide::find($id);
        $slide->delete();
        /*Xóa hình cũ đi khi ta đã chỉnh sửa để lưu hình mới lại*/
        unlink("upload/slide/".$slide->Hinh);
        return redirect('admin/slide/danhsach')->with('thongbao','Bạn đã xóa thành công Slide');
    }
}
