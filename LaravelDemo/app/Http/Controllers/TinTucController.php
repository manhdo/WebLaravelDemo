<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
use App\LoaiTin;
use App\TinTuc;
use App\Http\Controllers\Controller;/*Còn dùng import thư viện này là Controller đã bao gồm toàn bộ các thư viện.Trong đó có cả ValidateRequest.(Mà Trong ValidateRequest lại bao gồm thư viện Unique)*/
use App\Comment;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;/*Thư viện Hỗ trợ việc đăng nhập Auth trong laravel*/
class TinTucController extends Controller
{
    //
    public function getDanhSach(){
        //return view('admin.tintuc.danhsach');
        $tintuc = TinTuc::orderBy('id','DESC')->get();
        return view('admin.tintuc.danhsach',['tintuc'=>$tintuc]);/*truyền dữ liệu sang trang tintuc.danhsach*/
    }

    public function getThem(){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        return view('admin.tintuc.them',['theloai'=>$theloai,'loaitin'=>$loaitin]);
    }

    /*Thêm dữ liệu vào danh sách*/
    public function postThem(Request $request){
        $this->validate($request,[
            //'Ten' => 'required|min:3|max:100'
            'TieuDe' => 'required|unique:TinTuc,TieuDe|min:3',
            'LoaiTin' => 'required',
            'TomTat' => 'required',
            'NoiDung' => 'required'
        ],
            [
                'TieuDe.required'=>'Bạn chưa nhập Tiêu đề',
                'LoaiTin.required'=>'Bạn chưa nhập loại tin',
                'TomTat.required'=>'Bạn chưa nhập tóm tắt',
                'NoiDung.required'=>'Bạn chưa nhập nội dung',
                'TieuDe.unique'=> 'Tên thể loại đã tồn tại',
                'TieuDe.min' => 'Tiêu đề phải có ít nhất 3 kí tự',
            ]
        );
        $tintuc = new TinTuc();//Khởi tạo 1 đối tượng mới để lưu vào csdl
        $tintuc->TieuDe = $request->TieuDe;
        $tintuc->TieuDeKhongDau = changeTitle($request->TieuDe);
        $tintuc->idLoaiTin = $request->LoaiTin;
        $tintuc->TomTat = $request->TomTat;
        $tintuc->NoiDung =  $request->NoiDung;
        $tintuc->SoLuotXem = 0;
        $tintuc->NoiBat = $request->NoiBat;
        if($request->hasFile('Hinh')){
            $file = $request->file('Hinh');
            $duoi_anh = $file->getClientOriginalExtension();/*Kiểm tra đuôi mở rộng của ảnh(png,jpg) trc khi thêm*/
            if($duoi_anh != 'jpg' && $duoi_anh!='png' && $duoi_anh!='jpeg' && $duoi_anh != 'gif'){
                return redirect('admin/tintuc/them')->with('loi_anh','Bạn chỉ được upload đúng dạng file ảnh ,đuôi png, jpg,jpeg,gif');
            }
            $name = $file->getClientOriginalName();/*Lấy tên của hình ra*/
            $Hinh = str_random(4)."_".$name;
            while (file_exists("upload/tintuc/".$Hinh))/*Thêm vòng lặp while để random tránh việc 4 số đầu random_đến số tiếp theo vẫn bị trùng*/
            {
                $Hinh = str_random(4)."_".$name;
            }
            $file->move("upload/tintuc",$Hinh);
            $tintuc->Hinh = $Hinh;//Lưu lại $Hinh vào $tintuc
        }else{
            $tintuc->Hinh = "";
        }
        $tintuc->save();/*Lưu tin tức lại vào db*/
        return redirect('admin/tintuc/them')->with('thongbao','Bạn đã thêm tin tức thành công');
    }

    public function getSua($id){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        $tintuc = TinTuc::find($id);//Tìm thể loại có id = $id mình truyền vào;
        return view('admin.tintuc.sua',['tintuc'=>$tintuc,'theloai'=>$theloai,'loaitin'=>$loaitin]);
    }

    public function postSua(Request $request , $id){/*Lấy id về để biết sửa tại thể loại nào*/
        $tintuc = TinTuc::find($id);//Tìm tin tức có id = $id mình truyền vào;
        $this->validate($request,[
            //'Ten' => 'required|min:3|max:100'
            //'TieuDe' => 'required|unique:TinTuc,TieuDe|min:3',
            'TieuDe' => 'required:TinTuc,TieuDe|min:3',
            'LoaiTin' => 'required',
            'TomTat' => 'required',
            'NoiDung' => 'required'
        ],
            [
                'TieuDe.required'=>'Bạn chưa nhập Tiêu Đề',
                'LoaiTin.required'=>'Bạn chưa nhập Loại Tin',
                'TomTat.required'=>'Bạn chưa nhập Tóm tắt',
                'NoiDung.required'=>'Bạn chưa nhập Nội dung',
//                'TieuDe.unique'=> 'Tên tiêu đề đã tồn tại',
                'TieuDe.min' => 'Tiêu đề phải có ít nhất 3 kí tự',
            ]
        );
        $tintuc->TieuDe = $request->TieuDe;
        $tintuc->TieuDeKhongDau = changeTitle($request->TieuDe);
        $tintuc->idLoaiTin = $request->LoaiTin;
        $tintuc->TomTat = $request->TomTat;
        $tintuc->NoiDung =  $request->NoiDung;
        $tintuc->NoiBat = $request->NoiBat;
        if($request->hasFile('Hinh')){
            $file = $request->file('Hinh');
            $duoi_anh = $file->getClientOriginalExtension();/*Kiểm tra đuôi mở rộng của ảnh(png,jpg) trc khi thêm*/
            if($duoi_anh != 'jpg' && $duoi_anh!='png' && $duoi_anh!='jpeg' && $duoi_anh != 'gif'){
                return redirect('admin/tintuc/them')->with('loi_anh','Bạn chỉ được upload đúng dạng file ảnh ,đuôi png, jpg,jpeg,gif');
            }
            $name = $file->getClientOriginalName();/*Lấy tên của hình ra*/
            $Hinh = str_random(4)."_".$name;
            while (file_exists("upload/tintuc/".$Hinh))/*Thêm vòng lặp while để random tránh việc 4 số đầu random_đến số tiếp theo vẫn bị trùng*/
            {
                $Hinh = str_random(4)."_".$name;
            }
            $file->move("upload/tintuc",$Hinh);
        /*Xóa hình cũ đi khi ta đã chỉnh sửa để lưu hình mới lại*/
            unlink("upload/tintuc/".$tintuc->Hinh);
            $tintuc->Hinh = $Hinh;//Lưu lại $Hinh vào $tintuc
        }/*Không xét else vì nếu người dùng ko muốn upload hình ảnh thì ta ko cho chạy if và đồng thời ko gán else như phía add tin tức như trên vì nếu else có thể sẽ gán rỗng cho ảnh*/
        $tintuc->save();/*Lưu tin tức lại vào db*/
        return redirect('admin/tintuc/sua/'.$id)->with('thongbao','Bạn đã sửa tin tức thành công');
    }

    public function getXoa($id){
        $tintuc = TinTuc::find($id);//Tìm thể loại có id = $id mình truyền vào;
        $comment = Comment::where('idTinTuc',$id); //Tìm đến bài viết có idTinTuc để xóa tin tức đó đi rồi ms xóa tin tức
        $comment->delete(); //Xóa tin tức có trong bảng comment
        $tintuc -> delete();
        /*Xóa hình cũ đi khi ta đã chỉnh sửa để lưu hình mới lại*/
        unlink("upload/tintuc/".$tintuc->Hinh);
        return redirect('admin/tintuc/danhsach')->with('thongbao','Bạn đã xóa thành công');
    }
}
