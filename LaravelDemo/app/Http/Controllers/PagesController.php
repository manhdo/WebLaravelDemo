<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\TheLoai;
use App\Slide;
use App\LoaiTin;
use App\TinTuc;
use Illuminate\Support\Facades\Auth;/*Thư viện Hỗ trợ việc đăng nhập Auth trong laravel*/
use App\User;
class PagesController extends Controller
{

    function __construct(){
        $theloai = TheLoai::all();
        $slide = Slide::all();
        view()->share('theloai', $theloai);
        view()->share('slide', $slide);
        /*check xem người dùng có đăng nhập hay không*/
        if(Auth::check()){
            view()->share('nguoidung', Auth::user());
        }
    }

    function trangchu(){
        //return view('pages.trangchu');
        //$theloai = TheLoai::all();
        return view('pages.trangchu');
    }
    function contact(){
        return view('pages.contact');
    }
    function loaitin($id){
        $loaitin = LoaiTin::find($id);
        $tintuc = TinTuc::where('idLoaiTin', $id)->paginate(5);/*Phân trang lấy 5 tin /1 trang*/
        return view('pages.loaitin',['loaitin'=>$loaitin, 'tintuc'=>$tintuc]);
    }

    function tintuc($id){
        $tintuc = TinTuc::find($id);
        /*Cách lấy tin nổi bật*/
        $tinnoibat = TinTuc::where('NoiBat',1)->take(4)->get();/*get take(4) ở đây chỉ lấy 4 tin*/
        $tinlienquan = TinTuc::where('idLoaiTin',$tintuc->idLoaiTin)->take(4)->get();/*Lấy các loại tin có cùng idLoaiTin*/
        return view('pages.detail',['tintuc'=>$tintuc, 'tinnoibat'=>$tinnoibat, 'tinlienquan'=>$tinlienquan]);
    }

    /*Đăng nhập*/
    function getLogin_user(){
        return view('pages.login_user');
    }
    function postLogin_user(Request $request){
        $this->validate($request,[
            'email' => 'required|email|',
            'password'=>'required|min:3|max:32',
        ],
            [
                'email.required'=>'Bạn chưa nhập email người dùng',
                'email.email'=>'Bạn chưa nhập đúng định dạng email',
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'Mật khẩu phải có ít nhất 3 kí tự',
                'password.max'=>'Mật khẩu chỉ được tối đa 32 kí tự',
            ]);
        if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password])){
            return redirect('trangchu');
        }else{
            return redirect('login_user')->with('thongbao','Đăng nhập không thành công');
        }
    }
    /*Đăng xuất*/
    function getLogout_user(){
        Auth::logout();
        return redirect('trangchu');
    }
    /*-------Chi tiết người dùng-----------*/
    function getNguoidung(){
        //$user = Auth::user();
        //return view('pages.nguoidung',['user'=>$user]);
    /*Do đã khai báo trên hàm construct nên ko cần phải khai báo lại*/
        return view('pages.nguoidung');
    }
    /*-----------Cho user chỉnh sửa người dùng-----------------*/
    function postNguoidung(Request $request){
        $this->validate($request,[
            'name' => 'min:3',
        ],
            [
                'name.min'=>'Tên người dùng phải có ít nhất 3 kí tự',
            ]);
        $user = Auth::user();
        $user->name = $request->name;
        if($request->changePassword == "on"){
            $this->validate($request,[
                'password'=>'required|min:3|max:32',
                'passwordAgain'=>'required|same:password'
            ],
                [
                    'password.required'=>'Bạn chưa nhập mật khẩu',
                    'password.min'=>'Mật khẩu phải có ít nhất 3 kí tự',
                    'password.max'=>'Mật khẩu chỉ được tối đa 32 kí tự',
                    'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                    'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp mật khẩu trên',
                ]);
            $user->password =  bcrypt($request->password);
        }
        $user->save();
        return redirect('nguoidung')->with('thongbao','Sửa thành công');
    }

    /*---------Trang giới thiệu--------------*/
    function getGioiThieu(){
        return view('pages.gioithieu');
    }
    /*----------Trang đăng ký-------------*/
    function getDangKy(){
        return view('pages.dangky');
    }
    public function postDangKy(Request $request){
        $this->validate($request,[
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users,email',
            'password'=>'required|min:3|max:32',
            'passwordAgain'=>'required|same:password'
        ],
            [
                'name.required'=>'Bạn chưa nhập tên người dùng',
                'name.min'=>'Tên người dùng phải có ít nhất 3 kí tự',
                'email.required'=>'Bạn chưa nhập email người dùng',
                'email.email'=>'Bạn chưa nhập đúng định dạng email',
                'email.unique'=>'Email đã tồn tại',
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'Mật khẩu phải có ít nhất 3 kí tự',
                'password.max'=>'Mật khẩu chỉ được tối đa 32 kí tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp mật khẩu trên',
            ]);
        $user= new User();
        $user->name = $request->name;
        $user->email =  $request->email;
        $user->password =  bcrypt($request->password);
        $user->quyen = 0;
        $user->save();
        return redirect('dangky')->with('thongbao','Thêm user thành công');
    }

    /*Tìm kiếm*/
    function getTimKiem(Request $request){
        //return view('pages.timkiem');
        $tukhoa = $request->tukhoa;
        $tintuc_tk = TinTuc::where('TieuDe','like',"%$tukhoa%")->orWhere('TomTat','like',"%$tukhoa%")->orWhere('NoiDung','like',"%$tukhoa%")->limit(30)->paginate(5);
        //return view('pages.timkiem',['tintuc_tk'=>$tintuc_tk,'tukhoa'=>$tukhoa]);
        if($tintuc_tk->count() > 0){
            return view('pages.timkiem',['tintuc_tk'=>$tintuc_tk,'tukhoa'=>$tukhoa]);
        }else{
            return view('pages.loiTimKiem',['tukhoa'=>$tukhoa]);
        }
    }
}
