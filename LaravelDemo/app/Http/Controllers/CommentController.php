<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\TheLoai;
use App\LoaiTin;
use App\TinTuc;
use App\Http\Controllers\Controller;/*Còn dùng import thư viện này là Controller đã bao gồm toàn bộ các thư viện.Trong đó có cả ValidateRequest.(Mà Trong ValidateRequest lại bao gồm thư viện Unique)*/
use App\Comment;
use App\User;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    //
    public function getXoa($id,$idTinTuc){
        $comment = Comment::find($id);
        $comment->delete();
        return redirect('admin/tintuc/sua/'.$idTinTuc)->with('thongbao_comment','Bạn đã xóa comment thành công');/*Truyền vào idTinTuc để hiển thị lại trang sửa tin tức theo id*/
    }
    /*Đưa commnet lên csdl*/
    public function postComment($id, Request $request){
        $idTinTuc = $id;
        $tintuc = TinTuc::find($id);
        $commnet = new Comment();
        $commnet->idTinTuc = $idTinTuc;
        $commnet->idUser = Auth::user()->id;
        $commnet->NoiDung = $request->NoiDung;
        $commnet->save();
        return redirect("tintuc/$id/".$tintuc->TieuDeKhongDau.".html")->with('thongbao','Bạn đã comment bài viết thành công');
    }
}
