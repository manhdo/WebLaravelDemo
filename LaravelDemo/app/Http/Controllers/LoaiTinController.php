<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
use App\LoaiTin;
//use Illuminate\Validation\Rules\Unique;/*Dùng cái này rõ ràng cho việc check Unique.Còn dùng ValidateRequests rõ ràng cho việc check validate input nhập vào*/
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;/*Còn dùng import thư viện này là Controller đã bao gồm toàn bộ các thư viện.Trong đó có cả ValidateRequest.(Mà Trong ValidateRequest lại bao gồm thư viện Unique)*/

class LoaiTinController extends Controller
{
    //
    public function getDanhSach(){
        //return view('admin.theloai.danhsach');
        $loaitin = LoaiTin::all();//Hàm lấy tất cả các loại tin là hàm all(); chúng ta ko cần phải select truy vấn nữa mà model nó đã thực hiện hết cho chúng ta
        return view('admin.loaitin.danhsach',['loaitin'=>$loaitin]);/*truyền dữ liệu sang trang loaitin.danhsach*/
    }

    /*Trả về trang them trong loaitin khi người dùng ấn vào click them*/
    public function getThem(){
        $theloai = TheLoai::all();
        return view('admin.loaitin.them',['theloai'=>$theloai]);
    }

    /*Thêm dữ liệu vào danh sách*/
    public function postThem(Request $request){
//        echo $request->Ten;
        $this->validate($request,[
            //'Ten' => 'required|min:3|max:100'
            'Ten' => 'required|unique:TheLoai,Ten|min:3|max:100',
            'TheLoai' => 'required'
        ],
            [
                'Ten.required'=>'Bạn chưa nhập thể loại',
                'Ten.unique'=> 'Tên thể loại đã tồn tại',
                'Ten.min' => 'Tên thể loại phải có độ dài từ 3 đến 100 kí tự',
                'Ten.max' => 'Tên thể loại phải có độ dài từ 3 đến 100 kí tự',
                'TheLoai.required'=>'Bạn chưa nhập thể loại',
            ]
        );
        $loaitin = new LoaiTin();//Khởi tạo 1 đối tượng mới để lưu vào csdl
        $loaitin->Ten = $request->Ten;
        $loaitin->TenKhongDau = changeTitle($request->Ten);
        $loaitin->idTheLoai = $request->TheLoai;
        //echo  $request->TheLoai;
        //echo changeTitle($request->Ten);
        $loaitin->save();/*Lưu thể loại lại*/
        return redirect('admin/loaitin/them')->with('thongbao','Bạn đã thêm thành công');
    }

    public function getSua($id){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::find($id);//Tìm thể loại có id = $id mình truyền vào;
        return view('admin.loaitin.sua',['loaitin'=>$loaitin,'theloai'=>$theloai]);
    }

    public function postSua(Request $request , $id){/*Lấy id về để biết sửa tại thể loại nào*/
        $loaitin = LoaiTin::find($id);//Tìm thể loại có id = $id mình truyền vào;
        $this->validate($request,[
                'Ten' => 'required|unique:TheLoai,Ten|min:3|max:100',
                'TheLoai' => 'required'
            ],
            [
                'Ten.required'=>'Bạn chưa nhập thể loại',
                'Ten.unique'=> 'Tên thể loại đã tồn tại',
                'Ten.min' => 'Tên thể loại phải có độ dài từ 3 đến 100 kí tự',
                'Ten.max' => 'Tên thể loại phải có độ dài từ 3 đến 100 kí tự',
                'TheLoai.required'=>'Bạn chưa nhập thể loại',
            ]);

        $loaitin->Ten = $request->Ten;
        $loaitin->TenKhongDau = changeTitle($request->Ten);
        $loaitin->idTheLoai = $request->TheLoai;/*Trong bảng loại tin đã lưu idTheLoai*/
        $loaitin->save();/*Lưu thể loại lại*/
        return redirect('admin/loaitin/sua/'.$id)->with('thongbao','Bạn đã sửa thành công');
    }

    public function getXoa($id){
        $loaitin = LoaiTin::find($id);//Tìm thể loại có id = $id mình truyền vào;
        $loaitin -> delete();
        return redirect('admin/loaitin/danhsach')->with('thongbao','Bạn đã xóa thành công');
    }
}
