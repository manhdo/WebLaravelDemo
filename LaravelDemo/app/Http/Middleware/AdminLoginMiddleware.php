<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;/*Thư viện Hỗ trợ việc đăng nhập Auth trong laravel*/
use Closure;

class AdminLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user = Auth::user();
            if($user->quyen == 1){
                return $next($request);/*Truy cập vào trang admin*/
            }else{
                return redirect('admin/login');
            }
        }else{
            return redirect('admin/login');
        }

    }
}
