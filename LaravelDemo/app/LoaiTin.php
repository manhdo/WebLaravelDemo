<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiTin extends Model
{
    //
    protected $table = "LoaiTin";
    public function theloai(){
        return $this->belongsTo('App\TheLoai','idTheLoai','id');
    }
    /*Muốn biết trong loại tin có bao nhiêu tin tuc*/
    public function tintuc(){
        return $this->hasMany('App\TinTuc','idLoaiTin','id');
    }
}
