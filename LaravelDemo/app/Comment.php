<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table = "Comment";
    public function tintuc(){
        return $this->belongsTo('App\TinTuc','idTinTuc','id');
    }
    /*Muốn biết comment thuộc người dùng nào.
        1 comment thuộc 1 user.và 1 user có thể có nhiều comment
    */
    public function user(){
        return $this->belongsTo('App\User','idUser','id');
    }

}
