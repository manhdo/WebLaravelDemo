<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Manh Do | @yield('title')</title>
{{--Cách xử lý khai báo đường dẫn mặc định cho (css/js) asset--}}
    <base href="{{asset('')}}">
    <!-- Bootstrap Core CSS -->
    <link href="user_asset/css/bootstrap.min.css" rel="stylesheet">

    @yield('css')
    <!-- Custom CSS -->
    <link href="user_asset/css/shop-homepage.css" rel="stylesheet">
    <link href="user_asset/css/my.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,500,700&amp;subset=latin,vietnamese" rel="stylesheet" type="text/css">
    <![endif]-->

</head>

<body>

    @include('layout.header')

    @yield('content')

    @include('layout.footer')

<!-- jQuery -->
    <base href="{{asset('')}}">
<script src="user_asset/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="user_asset/js/bootstrap.min.js"></script>
<script src="user_asset/js/my.js"></script>



@yield('script')
</body>

</html>
