{{--Chỉ đưa ra những thể loại mà có loại tin bên trong.Cái nào không có thì ta sẽ không in thể loại đó ra--}}
<div class="col-md-3 ">
    <ul class="list-group" id="menu">
        <li href="#" class="list-group-item menu1 active">
            Menu
        </li>
        @foreach($theloai as $tl)
            @if(count($tl->loaitin)>0)
                <li href="#" class="list-group-item menu1">
                    {{$tl->Ten}}
                </li>
                <ul>
                    @foreach($tl->loaitin as $lt)
                        <li class="list-group-item">
                            <a href="loaitin/{{$lt->id}}/{{$lt->TenKhongDau}}.html">{{$lt->Ten}}</a>
                        </li>
                    @endforeach
                </ul>
            @endif
        @endforeach
    </ul>
    <br/>

    <div style="margin-bottom: 15px;">
        <img class="img-responsive" src="upload/tintuc/ads.png">
    </div>
    <br/>
    <div style="margin: 15px 0px;margin-top: 50px;">
        <img class="img-responsive" src="upload/tintuc/ads2.png">
    </div>
    <div class="tichhop_fb" style="margin-top: 200px;margin-bottom: 50px;">
        <h2>Blog Facebook</h2>
        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fketnoitienganh%2F&tabs=timeline&width=284&height=350&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1615151565418065" width="284" height="350" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
    </div>
</div>

