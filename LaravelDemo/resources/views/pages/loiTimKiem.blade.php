@extends('layout.index')
@section('content')
    <!-- Page Content -->
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            @include('layout.menu')

            <div class="col-md-9 ">
                <div class="panel panel-default" style="min-height:600px;">
                    <div class="panel-heading" style="background-color:#337AB7; color:white;">
                        <h4><b>Tìm kiếm : {{$tukhoa}}</b></h4>
                    </div>
                    {{----Thông báo lỗi tìm kiếm------}}

                        <div class="alert alert-danger">
                            <h3>Không tìm thấy tin tức liên quan đến từ khóa : {{$tukhoa}} </h3>
                        </div>

                </div>
            </div>

        </div>

    </div>
    <!-- end Page Content -->
@endsection
