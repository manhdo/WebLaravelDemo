@extends('layout.index')
@section('title')
   Title ManhDo
@endsection
@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-9">

                <!-- Blog Post -->

                <!-- Title -->
                <h2>{{$tintuc->TieuDe}}</h2>

                <!-- Author -->
                <p class="lead">
                    By: <a href="#">admin</a>
                </p>

                <!-- Preview Image -->
                <img class="img-responsive" src="upload/tintuc/{{$tintuc->Hinh}}" alt="">

                <!-- Date/Time -->
                <p style="margin: 15px 0px 10px 0px;"><span class="glyphicon glyphicon-time"></span>Posted on : {{$tintuc->created_at}}</p>
                <hr>

                <!-- Post Content -->
                <p class="lead">
                    {!! $tintuc->NoiDung !!}
                    {{--Phải dùng {!!  !!} do bên trong chứa thẻ html khi in nội dung vào csdl--}}
                </p>
                <hr>
                <!-- Blog Comments -->
                <!-- Comments Form -->
                @if(Auth::user())
                    <div class="well">
                        {{----Thông báo thêm thành công------}}
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <h4>Viết bình luận ...<span class="glyphicon glyphicon-pencil"></span></h4>
                        <form role="form" action="comment/{{$tintuc->id}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <div class="form-group">
                                <textarea class="form-control" name="NoiDung" rows="3"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Gửi</button>
                        </form>
                    </div>
                @endif
                <hr>

                <!-- Posted Comments -->
                @foreach($tintuc->comment as $cm)
                    <!-- Comment -->
                    <div class="media">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="http://placehold.it/64x64" alt="">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">{{$cm->user->name}}
                                <small>{{$cm->created_at}}</small>
                            </h4>
                             {{$cm->NoiDung}}
                        </div>
                    </div>
                @endforeach
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-3">

                <div class="panel panel-default">
                    <div class="panel-heading"><b>Tin liên quan</b></div>
                    <div class="panel-body">
                        @foreach($tinlienquan as $tt)
                            <!-- item -->
                            <div class="row" style="margin-top: 10px;">
                                    <div class="col-md-5" style="padding-right: 8px; padding-left: 8px;">
                                        <a href="tintuc/{{$tt->id}}/{{$tt->TieuDeKhongDau}}.html">
                                            <img class="img-responsive" src="upload/tintuc/{{$tt->Hinh}}" alt="{{$tt->Hinh}}">
                                        </a>
                                    </div>
                                    <div class="col-md-7"  style="padding-right: 8px;padding-left: 8px;">
                                        <a href="tintuc/{{$tt->id}}/{{$tt->TieuDeKhongDau}}.html"><p style="font: 700 14px arial;color: #333;">{{$tt->TieuDe}}</p></a>
                                    </div>
                                     <div class="col-md-12" style="padding: 5px 10px;">
                                         <a href="tintuc/{{$tt->id}}/{{$tt->TieuDeKhongDau}}.html">
                                            <span id="wrapper_dotdot" style="font-size: 12px;">{!! $tt->TomTat !!}</span>
                                         </a>
                                     </div>
                                <div class="break"></div>
                            </div>
                            <!-- end item -->
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><b>Tin nổi bật</b></div>
                    <div class="panel-body">
                        @foreach($tinnoibat as $tn)
                            <!-- item -->

                                <div class="row" style="margin-top: 10px;">
                                        <div class="col-md-5" style="padding-right: 8px; padding-left: 8px;">
                                            <a href="tintuc/{{$tn->id}}/{{$tn->TieuDeKhongDau}}.html">
                                                <img class="img-responsive" src="upload/tintuc/{{$tn->Hinh}}" alt="{{$tn->Hinh}}" >
                                            </a>
                                        </div>
                                        <div class="col-md-7" style="padding-right: 8px;padding-left: 8px;">
                                            <a href="tintuc/{{$tn->id}}/{{$tn->TieuDeKhongDau}}.html"><p style="font: 700 14px arial;color: #333;">{{$tn->TieuDe}}</p></a>
                                        </div>
                                        <div class="col-md-12" style="padding: 5px 10px;">
                                            <a href="tintuc/{{$tn->id}}/{{$tn->TieuDeKhongDau}}.html">
                                                <span id="wrapper_dotdot" style="font-size: 12px;">{!! $tn->TomTat !!}</span>
                                            </a>
                                        </div>
                                    <div class="break"></div>
                                </div>
                            <!-- end item -->
                        @endforeach
                    </div>
                </div>
                <div class="panel panel-default">
                    <div >
                        <img class="img-responsive" src="upload/tintuc/ads2.png">
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->
    </div>
    <!-- end Page Content -->
    {{--jquery--}}
    <script>
        $(document).ready(function() {
            $("#wrapper_dotdot").dotdotdot({
                /*	The text to add as ellipsis. */
                ellipsis: '... '
            });
        });
    </script>
    {{--Jquery dotdot ẩn chữ quá dài  --}}
    <script src="user_asset/js/jquery.js" type="text/javascript"></script>
    <script src="user_asset/js/jquery.dotdotdot.js" type="text/javascript"></script>
@endsection