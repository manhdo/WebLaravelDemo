@extends('layout.index')
@section('content')
    <!-- Page Content -->
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            @include('layout.menu')

            <div class="col-md-9 ">
                <div class="panel panel-default" style="min-height:600px;">
                    <div class="panel-heading" style="background-color:#337AB7; color:white;">
                        <h4><b>Tìm kiếm : {{$tukhoa}}</b></h4>
                    </div>
                    {{----Thông báo thêm thành công------}}
                    @if(session('thongbao_tk'))
                        <div class="alert alert-success">
                            {{session('thongbao_tk')}}
                        </div>
                    @endif

                    @foreach($tintuc_tk as $tt)
                        <div class="row-item row">
                            <div class="col-md-3">
                                <a href="detail.html">
                                    <br>
                                    <img width="200px" height="200px" class="img-responsive" src="upload/tintuc/{{$tt->Hinh}}" alt="">
                                </a>
                            </div>
                            <div class="col-md-9">
                                <h3>{{$tt->TieuDe}}</h3>
                                <p>{!!$tt->TomTat  !!}</p>
                                <a class="btn btn-primary" href="tintuc/{{$tt->id}}/{{$tt->TieuDeKhongDau}}.html">Xem chi tiết <span class="glyphicon glyphicon-chevron-right"></span></a>
                            </div>
                            <div class="break"></div>
                        </div>
                    @endforeach

                <!-- Pagination -->
                    {{--phân trang trong Laravel--}}
                    {{--{{$tintuc_tk->links()}}--}}
                    {!! $tintuc_tk->appends(['tukhoa' =>$tukhoa])->links()!!}
                    <style>
                        .pagination{
                            margin-left: 15px;
                        }
                    </style>
                    <!-- /.row -->
                </div>
            </div>

        </div>

    </div>
    <!-- end Page Content -->
@endsection
