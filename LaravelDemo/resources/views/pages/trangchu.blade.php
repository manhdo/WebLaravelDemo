@extends('layout.index')
@section('content')
<!-- Page Content -->
<div class="container">

    @include('layout.slide')

    <div class="space20"></div>

    <div class="row main-left">
        @include('layout.menu')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#337AB7; color:white;" >
                    <h2 style="margin-top:0px; margin-bottom:0px;">Tin Hay Mỗi Ngày</h2>
                </div>
                <div class="panel-body">
                    <!-- item -->
                    @foreach($theloai as $tl)
                        {{--kiểm tra thể loại phải có loại tin (ít nhất 1 loại tin thì mới cho hiển thị ra)--}}
                        @if(count($tl->loaitin) >0 )
                              <div class="row-item row">
                                    <h3>
                                        <a href="category.html">{{$tl->Ten}}</a> |
                                        @foreach($tl->loaitin as $lt)
                                            <small><a href="loaitin/{{$lt->id}}/{{$lt->TenKhongDau}}.html"><i>{{$lt->Ten}}</i></a>/</small>
                                        @endforeach
                                    </h3>
                                    {{--Lấy ra 5 tin tức nổi bật nhất trong thể loại--}}
                                    @php
                                        $data = $tl->tintuc->where('NoiBat',1 )->sortByDesc('created_at')->take(6);
                                        $tin1 = $data->shift();/*Lấy ra 1 tin=>Còn lại có 4 tin trong $data(kiểu shift trả về mảng=> $tin1 đang ở dạng mảng)*/
                                    @endphp
                                    <div class="col-md-8 border-right">
                                        <div class="col-md-5">
                                            <a href="tintuc/{{$tin1['id']}}/{{$tin1['TieuDeKhongDau']}}.html">
                                                <img class="img-responsive" src="upload/tintuc/{{$tin1['Hinh']}}" alt="{{$tin1['Hinh']}}">
                                            </a>
                                        </div>

                                        <div class="col-md-7">
                                            <h3>{{$tin1->TieuDe}}</h3>
                                            <p>{!! $tin1->TomTat !!}</p>
                                            <a class="btn btn-primary" href="tintuc/{{$tin1['id']}}/{{$tin1['TieuDeKhongDau']}}.html">Xem chi tiết <span class="glyphicon glyphicon-chevron-right"></span></a>
                                        </div>
                                    </div>


                                    <div class="col-md-4" style="padding-right:8px; padding-left: 8px; ">
                                        @foreach($data->all() as $tintuc)
                                            <div class="item_tintuc" style="margin-bottom: 10px;">
                                                <a href="tintuc/{{$tintuc['id']}}/{{$tintuc['TieuDeKhongDau']}}.html">
                                                    <div class="col-md-4" style="padding-left: 5px;padding-right: 5px;">
                                                        {{--<span style="font-size: 30px;" class="glyphicon glyphicon-list-alt"></span>--}}
                                                        {{--<i class="fa fa-newspaper-o" aria-hidden="true" style="font-size: 30px;"></i>--}}
                                                        <img class="img-responsive" src="upload/tintuc/{{$tintuc['Hinh']}}" alt="{{$tintuc['Hinh']}}">
                                                    </div>
                                                    <div class="col-md-8" style="padding-left: 5px;padding-right: 5px;">
                                                        <h4 style="margin-top: 0px;">
                                                            {{--<span class="glyphicon glyphicon-list-alt"></span>--}}
                                                            {{$tintuc->TieuDe}}
                                                        </h4>
                                                    </div>
                                                </a>
                                                <div class="row"></div>
                                                {{--<div class="clearfix "></div>--}}
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="break"></div>
                              </div>
                               {{--@if($tl->count()==3)--}}
                                    {{--<div class="row-item row">--}}
                                        {{--<img class="img-responsive" src="upload/tintuc/ads.png">--}}
                                    {{--</div>--}}
                               {{--@endif--}}
                    @endif
                    @endforeach
                    <!-- end item -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- end Page Content -->
@endsection