@extends('layout.index')
@section('content')
    <!-- Page Content -->
    <div class="container">

        <!-- slider -->
        <div class="row carousel-holder" style="min-height:420px;margin-top: 50px; ">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Đăng nhập</div>
                    {{----Thông báo lỗi check validate-----}}
                    @if(count($errors) >0 )
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif
                    {{----Thông báo thêm thành công------}}
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    <div class="panel-body">
                        <form action="login_user" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <div>
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email" name="email" />
                            </div>
                            <br>
                            <div>
                                <label>Mật khẩu</label>
                                <input type="password" class="form-control" name="password" />
                            </div>
                            <br>
                            <button type="submit" class="btn btn-default">Đăng nhập</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
        <!-- end slide -->
    </div>
    <!-- end Page Content -->
@endsection